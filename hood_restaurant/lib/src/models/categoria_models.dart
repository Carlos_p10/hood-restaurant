
class Categorias {
    List<Categoria> data;
    bool status;

    Categorias({
        this.data,
        this.status,
    });

    factory Categorias.fromJson(Map<String, dynamic> json) => Categorias(
        data: List<Categoria>.from(json["data"].map((x) => Categoria.fromJson(x))),
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "status": status,
    };
}

class Categoria {

    bool estado;
    String id;
    String nombre;
    DateTime createdAt;
    DateTime updatedAt;
    int v;

    Categoria({
        this.estado,
        this.id,
        this.nombre,
        this.createdAt,
        this.updatedAt,
        this.v,
    });

    factory Categoria.fromJson(Map<String, dynamic> json) => Categoria(
        estado: json["estado"],
        id: json["_id"],
        nombre: json["nombre"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
    );

    Map<String, dynamic> toJson() => {
        "estado": estado,
        "_id": id,
        "nombre": nombre,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
    };
}
