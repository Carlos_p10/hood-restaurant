import 'dart:convert';
import 'package:hood_restaurant/src/models/user_model.dart';

Menus menusFromJson(String str) => Menus.fromJson(json.decode(str));

String menusToJson(Menus data) => json.encode(data.toJson());

class Menus {
    Menus({
        this.ok,
        this.msg,
        this.data,
    });

    bool ok;
    String msg;
    List<Menu> data;

    factory Menus.fromJson(Map<String, dynamic> json) => Menus(
        ok: json["ok"],
        msg: json["msg"],
        data: List<Menu>.from(json["data"].map((x) => Menu.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "ok": ok,
        "msg": msg,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class Menu {
    Menu({
        this.precio,
        this.tamanio,
        this.ingredientes,
        this.estado,
        this.numVotes,
        this.totalScore,
        this.rating,
        this.id,
        this.nombre,
        this.descripcion,
        this.usuario,
        this.createdAt,
        this.updatedAt,
        this.v,
    });

    int precio;
    String tamanio;
    List<String> ingredientes;
    bool estado;
    int numVotes;
    int totalScore;
    int rating;
    String id;
    String nombre;
    String descripcion;
    User usuario;
    DateTime createdAt;
    DateTime updatedAt;
    int v;

    factory Menu.fromJson(Map<String, dynamic> json) => Menu(
        precio: json["precio"],
        tamanio: json["tamanio"],
        ingredientes: List<String>.from(json["ingredientes"].map((x) => x)),
        estado: json["estado"],
        numVotes: json["num_votes"],
        totalScore: json["total_score"],
        rating: json["rating"],
        id: json["_id"],
        nombre: json["nombre"],
        descripcion: json["descripcion"],
        usuario: json["usuario"] == null ? null : User.fromJson(json["usuario"]),
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
    );

    Map<String, dynamic> toJson() => {
        "precio": precio,
        "tamanio": tamanio,
        "ingredientes": List<dynamic>.from(ingredientes.map((x) => x)),
        "estado": estado,
        "num_votes": numVotes,
        "total_score": totalScore,
        "rating": rating,
        "_id": id,
        "nombre": nombre,
        "descripcion": descripcion,
        "usuario": usuario == null ? null : usuario.toJson(),
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
    };
}
