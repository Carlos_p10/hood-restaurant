import 'dart:convert';

Details detailsFromJson(String str) => Details.fromJson(json.decode(str));

String detailsToJson(Details data) => json.encode(data.toJson());

class Details {
    Details({
        this.ok,
        this.msg,
        this.data,
    });

    bool ok;
    String msg;
    List<Detail> data;

    factory Details.fromJson(Map<String, dynamic> json) => Details(
        ok: json["ok"],
        msg: json["msg"],
        data: List<Detail>.from(json["data"].map((x) => Detail.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "ok": ok,
        "msg": msg,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class Detail {
    Detail({
        this.cantidad,
        this.estado,
        this.status,
        this.id,
        this.empresa,
        this.menu,
        this.direccion,
        this.usuario,
        this.createdAt,
        this.updatedAt,
        this.v,
    });

    int cantidad;
    String estado;
    bool status;
    String id;
    Usuario empresa;
    Menu menu;
    Direccion direccion;
    Usuario usuario;
    DateTime createdAt;
    DateTime updatedAt;
    int v;

    factory Detail.fromJson(Map<String, dynamic> json) => Detail(
        cantidad: json["cantidad"],
        estado: json["estado"],
        status: json["status"],
        id: json["_id"],
        empresa: json["empresa"] == null ? null : Usuario.fromJson(json["empresa"]),
        menu: json["menu"] == null ? null : Menu.fromJson(json["menu"]),
        direccion: json["direccion"] == null ? null : Direccion.fromJson(json["direccion"]),
        usuario: Usuario.fromJson(json["usuario"]),
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
    );

    Map<String, dynamic> toJson() => {
        "cantidad": cantidad,
        "estado": estado,
        "status": status,
        "_id": id,
        "empresa": empresa == null ? null : empresa.toJson(),
        "menu": menu == null ? null : menu.toJson(),
        "direccion": direccion == null ? null : direccion.toJson(),
        "usuario": usuario.toJson(),
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
    };
}

class Direccion {
    Direccion({
        this.latitud,
        this.longitud,
        this.referencia,
        this.id,
        this.nombre,
        this.usuario,
        this.createdAt,
        this.updatedAt,
        this.v,
    });

    String latitud;
    String longitud;
    String referencia;
    String id;
    String nombre;
    String usuario;
    DateTime createdAt;
    DateTime updatedAt;
    int v;

    factory Direccion.fromJson(Map<String, dynamic> json) => Direccion(
        latitud: json["latitud"],
        longitud: json["longitud"],
        referencia: json["referencia"],
        id: json["_id"],
        nombre: json["nombre"],
        usuario: json["usuario"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
    );

    Map<String, dynamic> toJson() => {
        "latitud": latitud,
        "longitud": longitud,
        "referencia": referencia,
        "_id": id,
        "nombre": nombre,
        "usuario": usuario,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
    };
}

class Usuario {
    Usuario({
        this.nombre,
        this.id,
        this.usuario,
        this.correo,
        this.foto,
    });

    String nombre;
    String id;
    String usuario;
    String correo;
    String foto;

    factory Usuario.fromJson(Map<String, dynamic> json) => Usuario(
        nombre: json["nombre"],
        id: json["_id"],
        usuario: json["usuario"],
        correo: json["correo"],
        foto: json["foto"],
    );

    Map<String, dynamic> toJson() => {
        "nombre": nombre,
        "_id": id,
        "usuario": usuario,
        "correo": correo,
        "foto": foto,
    };
}

class Menu {
    Menu({
        this.precio,
        this.id,
        this.nombre,
        this.descripcion,
    });

    double precio;
    String id;
    String nombre;
    String descripcion;

    factory Menu.fromJson(Map<String, dynamic> json) => Menu(
        precio: json["precio"].toDouble(),
        id: json["_id"],
        nombre: json["nombre"],
        descripcion: json["descripcion"],
    );

    Map<String, dynamic> toJson() => {
        "precio": precio,
        "_id": id,
        "nombre": nombre,
        "descripcion": descripcion,
    };
}
