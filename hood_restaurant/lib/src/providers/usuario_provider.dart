import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:hood_restaurant/src/models/user_model.dart';
import 'package:hood_restaurant/src/preferences_user/preferences_user.dart';

class UsuarioProvider {
  Response resp;
  final dio = new Dio();
  final String _url = 'https://dev-hood.herokuapp.com/ws/auth';
  final String _urlUser = 'https://dev-hood.herokuapp.com/ws/user';
  final _prefs = new PreferenciasUsuario();

  final opts = Options(
      followRedirects: false,
      validateStatus: (status) {
        return status < 500;
      });

  Future<Map<String, dynamic>> login(String email, String pass) async {
    final authData = {
      'email': email,
      'pass': pass,
    };

    resp = await dio.post(_url, data: json.encode(authData), options: opts);

    Map<String, dynamic> decodedResp = resp.data;
    if (decodedResp['ok']) {
      _prefs.token = decodedResp['token'];
      return {'ok': true, 'token': decodedResp['token']};
    } else {
      return {'ok': false, 'mensaje': 'Usuario no existente'};
    }
  }

  Future<Map<String, dynamic>> nuevoUsuario(
      String nombre,
      String username,
      String direccion,
      String email,
      String pass,
      String pass2,
      String isSth) async {
    final authData = {
      'nombre': nombre,
      'usuario': username,
      'direccion': direccion,
      'correo': email,
      'contrasenia': pass,
      'r_contrasenia': pass2,
      'role': isSth
    };

    final String urlReg = "$_url/register";

    resp = await dio.post(urlReg, data: json.encode(authData), options: opts);

    Map<String, dynamic> decodedResp = resp.data;
    print(decodedResp);
    print(resp.statusCode);

    if (decodedResp.containsKey('data')) {
      _prefs.token = decodedResp['token'];
      return {'ok': true, 'token': decodedResp['token']};
    } else {
      return {'ok': false, 'mensaje': decodedResp['mensaje']};
    }
  }

  Future<List<User>> _procesarRespuesta(String url) async {
    final resp =
        await dio.get(url, options: opts, queryParameters: {"role": 'ent'});
    final decodedData = resp.data;
    final users = new Users.fromJson(decodedData);
    // print(users.msg);
    // print(users.data[0].nombre);
    return users.data;
  }

  Future<User> _procesarRespuestaN(String url) async {
    final resp = await dio.get(url, options: opts);
    final decodedData = resp.data;
    final users = new JUser.fromJson(decodedData);
    return users.data;
  }//muestra solo para un usuario

  Future<User> getUserById(String id) async {
    final url = '$_urlUser/$id';
    return await _procesarRespuestaN(url);
  }
}
