import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:hood_restaurant/src/models/detail_model.dart';

class DetailProvider {
  Response resp;
  final dio = new Dio();
  final String _url = 'https://dev-hood.herokuapp.com/ws/details';

  final opts = Options(
      followRedirects: false,
      validateStatus: (status) {
        return status < 500;
      });

  Future<List<Detail>> _procesarRespuesta(String url) async {
    final resp = await dio.get(url, options: opts);
    final decodedData = resp.data;
    final details = new Details.fromJson(decodedData);
    return details.data;
  }

  Future<List<Detail>> getDetail() async {
    final String url = '$_url/all';
    return await _procesarRespuesta(url);
  }


}
