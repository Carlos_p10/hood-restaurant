import 'package:flutter/material.dart';
import 'package:hood_restaurant/src/models/detail_model.dart';

class OrderDetail extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final Detail detail = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
                _backgroundLogo(),
                _titleRestaurant(detail),
                _cardItem(),
                _cardItem()
              ],
            ),
          ),
        )
      ),
    );
  }

  Widget _backgroundLogo(){
    return Container(
      width: double.infinity,
      color: Colors.black,
      height: 300,
      child: FadeInImage(
        fit: BoxFit.cover,
        placeholder: AssetImage('assets/images/loading.gif'),
        image: NetworkImage('https://dragoner.info/wp-content/uploads/2018/10/mzl.ctzgxbrx.jpg') ,
      ),
    );
  }
  Widget _titleRestaurant(Detail detail){
    return Container(
      padding: EdgeInsets.all(10),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(detail.empresa.nombre, style:TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.w600
          )),
          SizedBox(height: 10),
          Text('Mi lista', style: TextStyle(
            fontSize: 20,
            color: Colors.black45
          ),)
        ],
      ),
    );
  }
  Widget _cardItem(){
    Color color = Color(0xe9ecef);
    return Container(
      margin: EdgeInsets.all(10),
      width: double.infinity,
      
      child: Row(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                child: FadeInImage(
                  height: 200,
                  fit: BoxFit.cover,
                  width: 100,
                  placeholder: AssetImage('assets/images/loading.gif'),
                  image: NetworkImage('https://i.imgur.com/9ibjeXO.jpg'),
                ),
              )
            ],
          ),
          Expanded(
            child: Column(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(10),
                  height: 200,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Alitas 1', style: TextStyle(
                        fontSize: 20
                      )),
                      SizedBox(height: 5),
                      Container(
                        width: double.infinity,
                        height: 100,
                        child: Text('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                          overflow: TextOverflow.ellipsis,
                          maxLines: 6,
                          style: TextStyle(
                            color: Colors.black45
                          ),
                        ),
                      ),
                      SizedBox(height:20),
                      Container(
                        width:double.infinity,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              alignment: Alignment.center,
                              height: 30.0,
                              child: Text("Cantidad: 1", style: TextStyle(
                                fontSize: 20
                              )),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  
}