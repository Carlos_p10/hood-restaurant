import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hood_restaurant/src/models/detail_model.dart';
import 'package:hood_restaurant/src/models/user_model.dart';
import 'package:hood_restaurant/src/pages/OrderDescription.dart';
import 'package:hood_restaurant/src/providers/detail_provider.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFFAFAFA),
        elevation: 3,
        title: Center(
          child: Text(
            "Hood",
            style: GoogleFonts.muli(
              color: Colors.black,
              fontSize: 19,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        brightness: Brightness.light,
      ),
      body: ListView(
        padding: EdgeInsets.all(15),
        children: <Widget>[_crearListDetails()],
      ),
    );
  }

  Widget _crearListDetails() {
    final detailProvider = DetailProvider();
    return FutureBuilder(
      future: detailProvider.getDetail(),
      builder: (context, AsyncSnapshot<List> snapshot) {
        if (snapshot.hasData) {
          return _listFood(snapshot.data);
        } else {
          return Container(child: Image.asset('assets/images/loading.gif'));
        }
      },
    );
  } //arreglar este codigo para las ordenes ingresadas

  Widget _listFood(List<Detail> details) {
    return Container(
      margin: EdgeInsets.only(top: 10, bottom: 10),
      width: double.infinity,
      height: 700,
      child: ListView.builder(
        itemCount: details.length,
        itemBuilder: (context, i) => _cardFood(details[i]),
      ),
    );
  }

  Widget _cardFood(Detail detail) {
    final nombre = detail.usuario.nombre;
    final estado = detail.estado;
    Color color = Color(0x33ca7f);

    return Card(
      elevation: 10,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Column(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10)),
            child: FadeInImage(
              placeholder: AssetImage('assets/images/loading.gif'),
              image: NetworkImage('https://i.imgur.com/9ibjeXO.jpg'),
              fadeInDuration: Duration(milliseconds: 200),
            ),
          ),
          SizedBox(height: 5),
          Container(
            padding: EdgeInsets.all(15),
            child: Row(children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('Orden No. 304', style: TextStyle(fontSize: 20)),
                    SizedBox(height: 5),
                    Text('Cliente: $nombre',
                        style: TextStyle(color: Colors.black38, fontSize: 15)),
                    Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                      margin: EdgeInsets.symmetric(vertical: 10),
                      decoration: BoxDecoration(
                          color: Colors.grey[300],
                          borderRadius: BorderRadius.circular(20)),
                      child: Text(estado,
                          style: TextStyle(color: Colors.black45)),
                    ),
                    SizedBox(height: 10),
                    Container(
                      alignment: AlignmentDirectional.bottomEnd,
                      child: FlatButton(
                          color: Colors.black12,
                          onPressed: () {
                            setState(() {
                              Navigator.pushNamed(context, 'description', arguments: detail);
                            });
                          },
                          child: Text(
                            'Ver orden',
                            style: GoogleFonts.muli(
                              color: Colors.black,
                              fontSize: 15,
                              fontWeight: FontWeight.w500,
                            ),
                          )),
                    )
                  ],
                ),
              ),
            ]),
          )
        ],
      ),
    );
  }
}
