import 'package:flutter/material.dart';
import 'package:hood_restaurant/src/pages/Home.dart';
import 'package:hood_restaurant/src/pages/HomePage.dart';
import 'package:hood_restaurant/src/pages/OrderDescription.dart';
import 'package:hood_restaurant/src/pages/ProfileEditPage.dart';
import 'package:hood_restaurant/src/pages/login_page.dart';
import 'package:hood_restaurant/src/pages/registro_page.dart';
import 'package:hood_restaurant/src/pages/OrderDetail.dart';

Map<String, WidgetBuilder> getApplicationRoutes(){
  return<String, WidgetBuilder>{
    '/' : (BuildContext context) => HomePage(),
    'login': (BuildContext context) => LoginPage(),
    'registro': (BuildContext context) => RegistroPage(),
    'detailOrder' : (BuildContext context) => OrderDetail(),    
    'homePage' : (BuildContext context) => HomePage(),
    'description' : (BuildContext context) => OrderDescriptionPage(),
    'perfil' : (BuildContext context) => ProfileEditPage(),
  };
}